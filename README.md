# GitLab 
<img src="./pics/gitlab1.png" alt="" width="800"/>

## Einleitung
GitLab ist eine webbasierte DevOps Lifecycle-Plattform, die eine Git-Repository-Manager-Funktion mit CI/CD-Pipelines und Issue-Tracking-Funktionen kombiniert. Es wird als Open-Source-Community-Edition und als proprietäre Enterprise Edition angeboten, die zusätzliche Funktionen bietet.

---
## Einrichten
### Infra
- [x] Erstelle ein neuen Ordner auf deinem PC 
```bash
mkdir gitlab-test
```

- [x] Anmelden auf [GitLab](https://gitlab.com/users/sign_in)

### SSH
- [ ] SSH-Key hinzufügen
- Auf GitLab auf das Profilbild klicken & auf **Edit Profile** klicken
<img src="./pics/edit-profile.png" alt="" width="400"/>

- Auf SSH Keys klicken & dann ganz rechts auf **add new key** klicken
<img src="./pics/ssh-keys.png" alt="" width="400"/>

### Projekt - Repository
- [x] Neues Projekt erstellen / --> Projekt = Repository
- [x] Projekt clonen - Auf der Projektseite den Code fürs kopieren klicken
    dieser erstellt auch noch eine README.md Datei für das Projekt

### CI/CD
- [x] Erstelle die Pipeline in dem du in dem Projektordner eine `.gitlab-ci.yml` Datei erstellst
```yml
stages:
- build
- test
- deploy

build_job:
stage: build
script:
    - echo "Building the app"

test_job:
stage: test
script:
    - echo "Testing the app"

deploy_job:
stage: deploy
script:
    - echo "Deploying the app"
```

### Finale
- [x] Commiten & Pushen
```bash
git add .
git commit -m "CI konfigurattion mit .gitlab-ci.yml"
git push
```

- Überprüfen ob die Pipeline funktioniert hat unter **Builds** --> **Pipelines**
<img src="./pics/pipeline.png" alt="" width="400"/>


---

## Projekt Public stellen
- [x] Vom Gitlab Startbildschirm, klicke auf Groups und deinen User
<img src="./pics/public-groups.png" alt="" width="400"/>


- [x] Gehe in die Settings und dann auf General
<img src="./pics/general.png" alt="" width="400"/>


- [x] Setze die Gruppe auf Public
<img src="./pics/public.png" alt="" width="400"/>


- [x] Nun gehe zu deinem Projekt und klicke auf Settings --> General
<img src="./pics/general2.png" alt="" width="400"/>


- [x] Klappe das Fenster bei **Visibility, project features, permissions** auf und setze das Projekt auf Public
<img src="./pics/visibility.png" alt="" width="400"/>